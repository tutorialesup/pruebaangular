import { Component } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  items: Observable<any[]>;
  constructor(db: AngularFirestore, public afAuth: AngularFireAuth) {
    this.items = db.collection('items').valueChanges();
    this.imprime();
    

  }
  title = 'app';
  login() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }
  loginemail() {
    this.afAuth.auth.signInWithEmailAndPassword("e@gmail.com","aaa").catch( (error) => {
      console.log(error.code);
      console.log(error.message);

    });
    
  }
  singupemail() {
    //this.afAuth.auth.signInWithPopup(new firebase.auth.EmailAuthProvider());
    this.afAuth.auth.createUserWithEmailAndPassword("e@gmail.com","aaa").catch( (error) => {
      console.log(error.code);
      console.log(error.message);

    });
  } 
  loginfb() {
    var provider = new firebase.auth.FacebookAuthProvider();
    this.afAuth.auth.signInWithPopup(provider);
    this.imprime();
  }
  logout() {
    this.afAuth.auth.signOut();
  }
  imprime(){
    
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        // User is signed in.
        
        user.providerData.forEach(function (profile) {
          console.log("Sign-in provider: "+profile.providerId);
          console.log("  Provider-specific UID: "+profile.uid);
          console.log("  Name: "+profile.displayName);
          console.log("  Email: "+profile.email);
          console.log("  Photo URL: "+profile.photoURL);
          
        });
      } else {
        // No user is signed in.
        console.log('nel');
      }
    });
  }
}
